const koa = require( "koa" )
const path = require( "path" )
const static = require( "koa-static" )
const send = require( "koa-send" )
const app = new koa()

const { PORT = 3000 } = process.env

const indexPath = path.resolve( __dirname, "dist/index.html" )

app.use( static( path.resolve( __dirname, "dist" ), {
    gzip: true
} ) )

app.use( async ctx => {
    await send( ctx, indexPath )
} )

app.listen( PORT, () => {
    console.log( `listening on port ${PORT}` )
} )