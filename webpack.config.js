const path = require( "path" )
const extractCSS = require( 'mini-css-extract-plugin' )
const { env: { NODE_ENV: mode = 'development' } } = process

module.exports = {
  mode,
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
    historyApiFallback: true
  },
  plugins: [
    new extractCSS({
      filename: '[name].css',
      chunkFilename: '[id].css',
      ignoreOrder: false,
    }),
  ],
  module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: extractCSS.loader,
              options: {
                publicPath: '',
                hmr: mode === 'development',
              },
            },
            {
              loader: 'css-loader',
              options: {
                modules: true,
              }
            },
          ],
        }
      ]
    }
  };