import { stringify } from "query-string"

const makeRequest = ( method, url, queryParams = {}, body = null ) => {
    const query = stringify( queryParams )

    return new Promise( ( resolve, reject ) => {
        return fetch( `${url}?${query}`, {
            mode: 'cors',
            method,
            body: body ? JSON.stringify( body )
                : undefined
        } )
            .then( resp => resp.json() )
            .then( data => resolve( data ) )
            .catch( err => reject( err ) )
    } )
}

export const get = ( url, queryParams ) =>
    makeRequest( 'GET', url, queryParams )

export const post = ( url, body, queryParams ) =>
    makeRequest( 'POST', url, queryParams, body )