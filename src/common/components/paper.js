import React from "react"
import styles from "./paper.css"

const Paper = ( { children } ) => (
    <div className={styles.paper}>
        { children }
    </div>
)

export default Paper