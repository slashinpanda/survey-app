import React from "react"
import styles from "./loader.css"

const Loader = () => (
    <div className={styles.loaderContainer}>
        <div className={styles.loader} />
        <div className={styles.loaderText}>
            <span>
                Loading
            </span>
        </div>
    </div>
)

export default Loader;