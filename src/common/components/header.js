import React from "react"
import styles from "./header.css"

const Header = ( { children } ) => (
    <div className={styles.header}>
        { children }
    </div>
)

export default Header