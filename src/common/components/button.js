import React from "react"
import styles from "./button.css"

const Button = ( { type = "butotn", children, variant = "blue" } ) => (
    <button type={type} className={ [ styles.button, styles[variant] ].join( " " ) }>
        { children }
    </button>
)

export default Button