const initialState = {
    pending: false,
    rejected: false,
    completed: false,
    data: null,
}

export const Actions = {
    PENDING: 'pending',
    REJECTED: 'rejected',
    FULFILLED: 'fulfilled'
}

const apiReducer = ( state = initialState, { type, payload: data } ) => {
   
    switch ( type ) {
        case Actions.PENDING:
            return {
                ...state,
                pending: true,
                rejected: false,
                completed: false,
                data: null,
            }
        case Actions.REJECTED:
            return {
                ...state,
                pending: false,
                rejected: true,
                completed: false,
                data: null,
            }
        case Actions.FULFILLED:
            return {
                ...state,
                pending: false,
                rejected: false,
                completed: true,
                data
            }
        default:
            return { ...state }
    }
    
}

export default apiReducer