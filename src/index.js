import "./index.css"
import React from "react"
import { render } from "react-dom"
import { Router, Link } from "@reach/router"
import SurveyList from "./pages/home";
import Container from "./common/components/container";
import Header from "./common/components/header";
import Survey from "./pages/survey";

const container = document.querySelector("#app")

const App = () => {

    return (
        <>
            <Header>
                <Container>
                    <Link to="/">
                        Survey App
                    </Link>
                </Container>
            </Header>
            <Container>
                <Router>
                    <Survey path="/survey/:surveyId" />
                    <SurveyList default />
                </Router>
            </Container>
        </>
    )

}

render( <App/>, container )