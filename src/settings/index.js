const BASE_URL = `https://private-5fb3f-surveysmock.apiary-mock.com`

export const GET_SURVEY_LIST = `${BASE_URL}/api/surveys`
export const GET_SURVEY = id => `${BASE_URL}/api/surveys/${id}`
export const POST_SURVEY = id => `${BASE_URL}/api/surveys/${id}/completions`