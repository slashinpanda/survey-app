import React, {
    useReducer
} from "react"
import apiReducer, { Actions } from "../../common/hooks/useApi";
import { get } from "../../utils/fetch";
import { GET_SURVEY_LIST } from "../../settings";
import SurveyCard from "./components/surveyCard";
import Loader from "../../common/components/loader";

const SurveyList = () => {

    const [ { pending, rejected, completed, data }, dispatch ] = useReducer( apiReducer, {
        pending: true,
        completed: false,
        rejected: false,
        data: null
    } )

    React.useEffect( () => {

        dispatch( { type: Actions.PENDING } )
        get( GET_SURVEY_LIST )
            .then( payload => dispatch( { type: Actions.FULFILLED, payload } ) )
            .catch( err => dispatch( { type: Actions.REJECTED } ) )
    }, [])

    if ( pending )
        return <Loader />
    else if ( rejected )
        return <h1>Failed to fetch survey list</h1>
    else {

        const { surveys } = data

        return (
            <div>
                <h1>Survey list</h1>
                { 
                    surveys.map( survey => (
                        <SurveyCard { ...survey } key={survey.id} />
                    ) )
                }
            </div>
        )
    }
}

export default SurveyList