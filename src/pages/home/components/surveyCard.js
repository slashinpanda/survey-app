import React from "react"
import styles from "./surveyCard.css"
import Paper from "../../../common/components/paper";
import Button from "../../../common/components/button";
import { Link } from "@reach/router";

const SurveyCard = ( { id, title, tagline } ) => (
    <Paper>
        <div className={styles.surveyContainer}>
            <div className={styles.surveyTitle}>{ title }</div>
            <div className={styles.surveyDescription}>{ tagline }</div>
            <Link to={ `/survey/${id}` }>
                <Button>take survey</Button>
            </Link>
        </div>
    </Paper>
)

export default SurveyCard