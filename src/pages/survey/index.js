import React, {
    useReducer
} from "react"
import { Field, Formik } from "formik";
import { Link } from "@reach/router";
import apiReducer, { Actions } from "../../common/hooks/useApi";
import { get, post } from "../../utils/fetch";
import { GET_SURVEY, POST_SURVEY } from "../../settings";
import Loader from "../../common/components/loader";
import Question from "./components/question";
import Button from "../../common/components/button";
import SuccessNotification from "./components/success";
import styles from "./survey.css";

const SurveyList = ( { surveyId } ) => {

    const [ completed, updateCompleted ] = React.useState( false )

    const [ { pending, rejected, data }, dispatch ] = useReducer( apiReducer, {
        pending: true,
        completed: false,
        rejected: false,
        data: null
    } )

    const handleSubmit = React.useCallback( completion => {
        console.log( data )
        post( POST_SURVEY( surveyId ), { completion } )
            .then( ( { status } ) => {
                if ( status === 'ok' )
                    updateCompleted( true )
            } )
    }, [ surveyId ] )

    React.useEffect( () => {

        dispatch( { type: Actions.PENDING } )
        get( GET_SURVEY( surveyId ) )
            .then( payload => dispatch( { type: Actions.FULFILLED, payload } ) )
            .catch( err => dispatch( { type: Actions.REJECTED } ) )

    }, [ surveyId ])

    if ( completed ) {

        return <SuccessNotification />

    } else {

        if ( pending )
            return <Loader />
        else if ( rejected )
            return <h1>Failed to fetch survey list</h1>
        else {

            const { survey: { title, tagline, questions } } = data;

            return (
                <div>
                    <h1>{ title }</h1>
                    <h2>{ tagline }</h2>
                    <Formik onSubmit={ handleSubmit }>
                        { ( { handleSubmit } ) => {
                            return (
                                <form className={ styles.surveyForm } onSubmit={ handleSubmit }>
                                    { questions.map( ( question, index ) => {
                                        const { id } = question
                                        return (
                                            <Field key={id} name={ `questions.${index}` }>
                                                { ( { field: { name, value }, form: { setFieldValue } } ) => (
                                                    <Question setFieldValue={setFieldValue} name={name} value={value} key={id} {...question} />
                                                ) }
                                            </Field>
                                        )
                                    } ) }
                                    <div className={styles.surveyFormBottom}>
                                        <Link to="/">
                                            <Button variant="grey" type="submit">Back</Button>
                                        </Link>
                                        <Button type="submit">Submit</Button>
                                    </div>
                                </form>
                            )
                        } }
                    </Formik>
                </div>
            )
        }

    }
}

export default SurveyList