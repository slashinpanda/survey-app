import React from "react"
import styles from "./question.css"
import Paper from "../../../common/components/paper";

const Question = ( { id: question_id, title, setFieldValue, options, value: { value: questionValue } = { value: null }, name } ) => { 
    
    const handleChange = React.useCallback( e => {
        const { value } = e.currentTarget
        setFieldValue( name, {
            question_id,
            value
        } )
    }, [ name, setFieldValue ] )

    return (
        <Paper>
            <div className={styles.container}>
                <div className={styles.questionTitle}>{ title }</div>
                { options.map( option => (
                    <div key={option} className={styles.questionOption}>
                        <input id={`${name}:${option}`} required value={option} onChange={ handleChange } type="radio" name={name} checked={ questionValue === option } />
                        <label htmlFor={`${name}:${option}`}>
                            { option }
                        </label>
                    </div>
                ) ) }
            </div>
        </Paper>
    )
}

export default Question