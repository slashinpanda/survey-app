import React from "react"
import styles from "./success.css"
import { Link } from "@reach/router"
import Button from "../../../common/components/button";

const SuccessNotification = () => (
    <div className={ styles.notificationContainer } >
        <h1 className={ styles.notificationTitle }>Thanks for answering the survey!</h1>
        <Link to="/">
            <Button type="button">back to list</Button>
        </Link>
    </div>
)

export default SuccessNotification